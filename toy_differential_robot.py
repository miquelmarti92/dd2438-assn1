# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 16:36:37 2016

@author: kkalem
"""

class differential_robot:
    cid = -1
    baseHandle = -1
    rwHandle = -1
    lwHandle = -1
    
    #init function, Name's are the names given to objects in v-rep
    def __init__(self, cid, baseName, rwName, lwName):
        self.cid = cid        
        err, self.baseHandle = vrep.simxGetObjectHandle(cid, baseName, vrep.simx_opmode_oneshot_wait)
        err, self.rwHandle = vrep.simxGetObjectHandle(cid, rwName, vrep.simx_opmode_oneshot_wait)
        err, self.lwHandle = vrep.simxGetObjectHandle(cid, lwName, vrep.simx_opmode_oneshot_wait)
    
    #sets the right wheel's ratget velocity to the given value
    def setRightVelocity(self,Vtarget):
        err = vrep.simxSetJointTargetVelocity(self.cid, self.rwHandle, Vtarget, vrep.simx_opmode_streaming)
        if err == 0:
            return True
        else:
            return False
            
    #sets the left wheel's ratget velocity to the given value
    def setLeftVelocity(self,Vtarget):
        err = vrep.simxSetJointTargetVelocity(self.cid, self.lwHandle, Vtarget, vrep.simx_opmode_streaming)
        if err == 0:
            return True
        else:
            return False
    
    #returns the base's position as a vector [x,y,z]
    def getPosition(self):
        err, robotPos = vrep.simxGetObjectPosition(self.cid, self.baseHandle, -1, vrep.simx_opmode_streaming)
        if err == 0:
            return robotPos
        else:
            print 'Error:'+str(err)+' cant get pos (not a problem if the first streaming command)'
            return [0,0,0]
            
    #returns the base's orientation as a vector [alpha, beta, gamma]
    def getOrientation(self):
        err, robotOri = vrep.simxGetObjectOrientation(self.cid, self.baseHandle, -1, vrep.simx_opmode_streaming)
        if err == 0:
            return robotOri
        else:
            print 'Error:'+str(err)+' cant get orientation (not a problem if the first streaming command)'
            return [0,0,0]
            
            
### example usage in main: ###
'''
robot = differential_robot(cid, 'robot', 'right_wheel_joint', 'left_wheel_joint')
err, contHandleR = vrep.simxGetObjectHandle(cid, 'controllerR', vrep.simx_opmode_oneshot_wait)
err, contHandleL = vrep.simxGetObjectHandle(cid, 'controllerL', vrep.simx_opmode_oneshot_wait)

cid = startVrep()

err, robotHandle = vrep.simxGetObjectHandle(cid, 'robot', vrep.simx_opmode_oneshot_wait)
err, rwHandle = vrep.simxGetObjectHandle(cid, 'right_wheel_joint', vrep.simx_opmode_oneshot_wait)
err, lwHandle = vrep.simxGetObjectHandle(cid, 'left_wheel_joint', vrep.simx_opmode_oneshot_wait)



time.sleep(2) #switch to vrep
for i in range(500):
    #vrep.simxPauseCommunication(cid, True)
    
    err, posR = vrep.simxGetObjectPosition(cid, contHandleR, -1, vrep.simx_opmode_streaming)
    err, posL = vrep.simxGetObjectPosition(cid, contHandleL, -1, vrep.simx_opmode_streaming)
    
    xR, yR, zR = posR
    xL, yL, zL = posL
    
    robot.setRightVelocity(xR*5)
    robot.setLeftVelocity(-xL*5)
    
    
    #vrep.simxPauseCommunication(cid, False)
    #print 'robotPos:'+str(robotPos)
    time.sleep(0.1)

robot.setLeftVelocity(0)
robot.setRightVelocity(0)
stopVrep(cid)
'''