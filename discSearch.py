# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 20:44:45 2016

@author: kkalem
"""

import numpy as np
import matlabHandler as mh
import matplotlib.pyplot as plt


def manhattanDistance(pos1,pos2):
    return np.abs(pos2[0]-pos1[0]+pos2[1]-pos1[1])
    
def euclidDistance(pos1,pos2):
    return np.sqrt((pos2[0]-pos1[0])**2 + (pos2[1]-pos1[1])**2)
    
#p1 p2 define a line, pn is the rogue point
def pointToLineWith2PointsDistance(p1,p2,pn):
    dist = np.abs((p2[1]-p1[1])*pn[0] - (p2[0]-p1[0])*pn[1] + p2[0]*p1[1] - p2[1]*p1[0])/euclidDistance(p1,p2)
    return dist
    
class Node:
    
    pos = [-1,-1]
    cost = 9999
    parent = None
    
    def __init__(self,pos,cost,parent):
        self.pos = pos
        self.cost= cost
        self.parent= parent
        
    def toKey(self):
        return str(self.pos)
        
    def __str__(self):
        return str(self.pos)+"|"+str(self.cost)
        
    
def cmpNode(n1,n2):
    if n1.cost > n2.cost:
        return 1
    elif n1.cost == n2.cost:
        return 0
    else:
        return -1
        
def tracePath(node):
    current = node
    path = [current.pos]
    while current.parent != None:
        path.append(current.pos)
        current = current.parent
    return path
    
def growDiscrete(currentNode,goalNode,m,n):
    four = [[0,1],[0,-1],[1,0],[-1,0]]
    eight = four + [[-1, -1], [-1, 1], [1, -1], [1, 1]]
    sixteen = eight + [[1, 2], [1, -2], [-1, 2], [-1, -2], [2, 1], [2, -1],  [-2, 1], [-2, -1]]
    
    res = []
    
    if n == 4:
        nays = four
    elif n ==8:
        nays = eight
    else:
        nays = sixteen

    
    for nay in nays:
        newx = currentNode.pos[0]+nay[0]
        newy = currentNode.pos[1]+nay[1]
        if  newx >= 0 and newy >= 0 and newx < len(m[0,:]) and newy < len(m[:,0]):
            #print str([newx,newy])
            if m[newx,newy] != 1: #not a wall and not out of bounds
                newpos = [newx,newy]
                    
                if traceBlockade(currentNode.pos,newpos,m):
                    continue
                else:
                    newc = euclidDistance(goalNode.pos,newpos)+currentNode.cost+euclidDistance(currentNode.pos,newpos)
                    newnode = Node(newpos,newc,currentNode)
                    res.append(newnode)
    res.sort(cmp=cmpNode)  
    
    return res
    
    
def astar(s,g,m, n = 4):
    sn = Node([s[0],s[1]],0,None)
    gn = Node([g[0],g[1]],0,None)
    undiscovered = [sn]
    discovered = {}
    goalReached = False
    while len(undiscovered) > 0 and goalReached == False:
        #print '\n\n'
        undiscovered.sort(cmp=cmpNode)
        undiscovered.reverse()
        current = undiscovered.pop()
        while discovered.has_key(current.toKey()):
            #print 'CURRENT IN DISCOVERED  '+current.toKey()
            if len(undiscovered) == 0:
                return discovered
            else:
                current = undiscovered.pop()
        
        children = growDiscrete(current,gn,m,n)
        
        for child in children:
            if discovered.has_key(child.toKey()):
                #print 'CHILD IN DISCOVERED  ' + child.toKey()
                pass
            else:
                if child.pos[0] == gn.pos[0] and child.pos[1] == gn.pos[1]:
                    goalReached = True
                    return tracePath(child)
                else:
                    undiscovered.insert(0,child)
                    
        discovered[current.toKey()] = current
    return discovered
    
#returns the coordiantes for a point between p1 p2 at percentage away from p1
def tracePoint(p1,p2,percent):
    L = euclidDistance(p1,p2)
    l = percent * L
    a = (p2[0]-p1[0]) * (l/L) + p1[0]
    b = (p2[1]-p1[1]) * (l/L) + p1[1]
    return np.array([a,b])

#discritizes a given real-valued coordinate to discrete-space 
def discretize(p):
    a = p[0]-np.floor(p[0])
    b = p[1]-np.floor(p[1])
    
    if a >= 0.5:
        x = np.ceil(p[0])
    else:
        x = np.floor(p[0])
        
    if b >= 0.5:
        y = np.ceil(p[1])
    else:
        y = np.floor(p[1])
        
        
    return np.array([x,y]).astype(int)

#traces a path between p1,p2 on map m, if wall is found before reaching p2, returns true
def traceBlockade(p1,p2,m):
    blocked = False
    if euclidDistance(p1,p2) == np.sqrt(2): #8 neighbour test
        tone = [p1[0],p2[1]]
        ttwo = [p2[0],p1[1]]
        if m[tone[0],tone[1]] == 1 and m[ttwo[0],ttwo[1]] == 1:
            blocked = True
    else:
        for i in range(10):
            d = tracePoint(p1,p2,0.1*i)
            p = discretize(d)
            x = p[0]
            y = p[1]
            if m[x,y] == 1:
                blocked = True
    return blocked

               
#returns a numpy array of coordinates for the walls
def mapToWalls(m):
    walls = []
    for i in range(len(m[0,:])):
        for j in range(len(m[:,0])):
            if m[i,j] == 1:
                walls.append([i,j])
    return np.array(walls)
    
#plots the map as a scatterplot
def plotMap(m,s,g,path = None):
    axes = plt.gca()
    plt.axis('equal')
    plt.grid()
    axes.set_xlim([-1,1+len(m[0,:])])
    axes.set_ylim([-1,1+len(m[:,0])])
    
    walls = mapToWalls(m)
    plt.scatter(walls[:,0],walls[:,1],marker = 's')
    plt.scatter(s[0],s[1],marker = 'x')
    plt.scatter(g[0],g[1],marker = '+')
    if path != None:
        plt.scatter(path[:,0],path[:,1],marker = '.', color = 'r')

    plt.show()

    
#########
if __name__ == '__main__':
    s, g, m = mh.getDiscreteObstacleMap()
    res = astar(s,g,m,16)
    plotMap(m,s,g,np.array(res))
