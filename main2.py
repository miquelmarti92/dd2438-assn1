# -*- coding: utf-8 -*-
"""
Created on Fri Feb 19 22:27:10 2016

@author: kkalem
"""

import vrep
import sys
import matlabHandler as mh
import polySearch as ps
import waypoint as wp
import discSearch as ds
import numpy as np
import matplotlib.pyplot as plt
import time

if __name__ == "__main__":
    s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map1.mat') #assn2map1 or map2
    mh.draw2Dpolygons(x,y,edges,False)
    try:
        plt.scatter(customer[:,0],customer[:,1],marker = 's')
    except IndexError:
        pass
    plt.scatter(s[:,0],s[:,1],marker = 'x', color = 'g')
    plt.scatter(g[:,0],g[:,1],marker = 'v', color = 'r')
    for i in range(len(s)):
        plt.plot([s[i,0],g[i,0]],[s[i,1],g[i,1]])

    for i in range(len(s)):
        st = s[i]
        gt = g[i]

        visRes = ps.astar(st,gt,x,y,edges,button)
        visRes.reverse()
        visRes=visRes[0:-1]
        polygons = ps.polysToVerts(x,y,button)

        view = ps.TracingView(x,y,edges)
        model = wp.dyn_car(view,ps.__dyn_car_vmax, ps.__dyn_car_amax,ps.__dyn_car_phimax,ps.__dyn_car_La)
        model.dss.setPosition(ps.in3d(st))
        searchRes = ps.sampleSearch(st,gt,visRes,polygons,10,10,model)

        model = wp.dyn_car(view,ps.__dyn_car_vmax, ps.__dyn_car_amax,ps.__dyn_car_phimax,ps.__dyn_car_La)
        model.dss.setPosition(ps.in3d(st))
        model.view.reinit()
        a,path_length,vel1=model.followWaypoints(searchRes.tolist(),0,False)
        ps.drawPath(model.view.pos_trace)
        plt.scatter(searchRes[:,0],searchRes[:,1], color = 'r', marker = 'x')