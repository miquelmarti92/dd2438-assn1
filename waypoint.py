# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 12:37:31 2016

@author: Miquel
"""
#TODO: MVC dt and other parameters required for the model instantiation should be obtained regardless of view

import vrep
import time
import sys
import numpy as np
import polySearch as ps


__max_time_per_waypoint = 5
def maxtime():
    return __max_time_per_waypoint

def normalize(v):
    norm=np.linalg.norm(v)
    if norm==0:
       return v
    return v/norm
#################################################################
#################################################################
#################################################################

def startVrep():
    vrep.simxFinish(-1) # just in case, close all opened connections
    clientID = vrep.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to V-REP
    vrep.simxSynchronous(clientID,True)
    vrep.simxStartSimulation(clientID,vrep.simx_opmode_oneshot)
    if clientID != -1:
        return clientID
        print 'Connected'
    else:
        print 'Can not connect to v-rep'
        sys.exit('Connection failed')

def stopVrep(clientID):
    # Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
    vrep.simxGetPingTime(clientID)
    # Now close the connection to V-REP:
    vrep.simxFinish(clientID)

#initilazies the vrep connection, runs the given mainFunc, terminates the connection
def runInVrep(mainFunc):
    clientID = startVrep()
    mainFunc(clientID)
    stopVrep(clientID)

class vrep_view:
    cid = -1
    baseHandle = -1
    scaling=-1

    pos_trace = [] #to hold the position of a model in time
    ori_trace = []


    def __init__(self, cid, baseName, scaling):
        self.cid = cid
        err, self.baseHandle = vrep.simxGetObjectHandle(cid, baseName, vrep.simx_opmode_oneshot_wait)
        self.scaling = scaling

    def reinit(self):
        self.pos_trace = []
        self.ori_trace = []

    #returns the base's position as a vector [x,y,z]
    def getPosition(self):
        err, robotPos = vrep.simxGetObjectPosition(self.cid, self.baseHandle, -1, vrep.simx_opmode_oneshot_wait)
        if err == 0:
            return np.array([robotPos[0]/self.scaling,robotPos[1]/self.scaling,robotPos[2]])
        else:
            print 'Error: '+str(err)+' cannot get position'
            return [0,0,0]

    #returns the base's orientation as a vector [alpha, beta, gamma]
    def getOrientation(self):
        err, robotOri = vrep.simxGetObjectOrientation(self.cid, self.baseHandle, -1, vrep.simx_opmode_oneshot_wait)
        if err == 0:
            return robotOri
        else:
            print 'Error: '+str(err)+' cannot get orientation'
            return [0,0,0]

    def setPosition(self,p_vector):
        err = vrep.simxSetObjectPosition(self.cid, self.baseHandle, -1,np.array([self.scaling*p_vector[0],self.scaling*p_vector[1],p_vector[2]]), vrep.simx_opmode_oneshot)
        self.pos_trace.append(p_vector) #save the position for later
        self.syncSignal()
        if err != 0:
            print 'Error: '+str(err)+' cannot set position'

    def setOrientation(self,o_vector):
        err = vrep.simxSetObjectOrientation(self.cid, self.baseHandle, -1,np.array(o_vector), vrep.simx_opmode_oneshot)
        self.ori_trace.append(o_vector)
        if err != 0:
            print 'Error: '+str(err)+' cannot set orientation'

    def syncSignal(self):
        #Sync signal
        vrep.simxSynchronousTrigger(self.cid)

    def getTimeStep(self):
        err, dt = vrep.simxGetFloatingParameter(self.cid, vrep.sim_floatparam_simulation_time_step, vrep.simx_opmode_oneshot_wait)
        if err == 0:
            return dt
        else:
            print 'Error: '+str(err)+' cannot get sim time step'
            return [0,0,0]

    def drawPoints(self,points):
        for p in points:
            err,dummyhandle = vrep.simxCreateDummy(self.cid,0.1,None,vrep.simx_opmode_oneshot_wait)
            err = vrep.simxSetObjectPosition(self.cid, dummyhandle, -1,self.scaling*np.hstack([p,0]), vrep.simx_opmode_oneshot_wait)
            if err != 0:
                print 'Error: '+str(err)+' cannot draw point'

class model_base:
    position = -1
    orientation = -1
    dt = -1
    timer = -1

    #init function, Name's are the names given to objects in v-rep
    def __init__(self, position, orientation, dt):
        self.position=position
        self.orientation=orientation
        self.dt=dt
        self.timer = 0

    def setPosition(self,p_vector):
        self.position = np.array(p_vector)

    def setOrientation(self,o_vector):
        self.orientation = np.array(o_vector)

    def updatePosition(self,u_vector):
        self.position = np.array(self.position)+np.array(u_vector)
        self.timer+=self.dt

    def updateOrientation(self,u_vector):
        self.orientation = np.array(self.orientation)+np.array(u_vector)

class discrete_statespace:
    view = -1
    dss = -1

    def __init__(self,vrep_view):
        self.view=vrep_view
        self.dss=model_base(self.view.getPosition(),self.view.getOrientation(),self.view.getTimeStep())

    def followWaypoints(self,waypoints):
        dist = 0
        for p in waypoints:
            u_vec=np.array(p)-self.dss.position[0:2]
            dist+=np.linalg.norm(u_vec)
            self.dss.setPosition(np.hstack([p,self.dss.position[2]]))
            self.view.setPosition(self.dss.position)
        return dist


class kin_point:
    view = -1
    dss = -1
    max_velocity = -1
    reset_velocity = -1

    def __init__(self,vrep_view,max_velocity):
        self.view=vrep_view
        self.dss=model_base(self.view.getPosition(),self.view.getOrientation(),self.view.getTimeStep())
        self.max_velocity = max_velocity
        self.reset_velocity = self.max_velocity


    def followWaypoints(self,waypoints):
        step_size=self.max_velocity*self.dss.dt
        for p in waypoints:
            #Compute difference to actual waypoint
            u_vec=np.array(p)-self.dss.position[0:2]

            #Step in dpositionirection to new point
            while max(abs(u_vec))>0:
                #Limit max step size
                if np.linalg.norm(u_vec)>step_size:
                    u_vec=normalize(u_vec)*step_size
                #Update model position
                self.dss.updatePosition(np.hstack([u_vec,0]))
                #Update view position
                self.view.setPosition(self.dss.position)
                #Compute new target vector
                u_vec=np.array(p)-self.dss.position[0:2]
        #print(self.dss.timer)
        return self.dss.timer


class dyn_point:
    view = -1
    dss = -1
    velocity = -1
    max_velocity = -1
    max_accel = -1
    reset_velocity= -1

    def __init__(self,vrep_view,max_velocity,max_accel):
        self.view=vrep_view
        self.dss=model_base(self.view.getPosition(),self.view.getOrientation(),self.view.getTimeStep())
        self.velocity = [0,0]
        self.max_accel = max_accel
        self.max_velocity = max_velocity
        self.reset_velocity = max_velocity

    def followWaypoints(self,waypoints,stop_flag):
        i=1
        target_vel=self.max_accel*30/self.max_velocity
        ANG_THRESH=0.01
        for p in waypoints:
            #Vector to next waypoint
            u_vec=np.array(p)-self.dss.position[0:2]
            #Vector to previous waypoint
            if i>1:
                u_prev=np.array(waypoints[i-2])-self.dss.position[0:2]
            #Step in direction to new point
            while np.linalg.norm(u_vec)>np.linalg.norm(self.velocity)*self.dss.dt*2:
                #compute accel vector
                accel_vec= normalize(u_vec)*self.max_accel
                if i>1:
                    #Vector to prev point
                    decel_vec= normalize(u_prev)*self.max_accel
                else:
                    decel_vec=np.array([0,0])
                #Compute brake distance to next vel and last vel
                brake_dist=np.power(np.linalg.norm(self.velocity),2)/2./self.max_accel
                brake_dist2=np.power(np.linalg.norm(self.velocity)-target_vel,2)/2/self.max_accel
                brake_cond= abs(np.linalg.norm(u_vec))<=(brake_dist+np.linalg.norm(self.velocity)*self.dss.dt*1.)
                brake_cond2= abs(np.linalg.norm(u_vec))<=(brake_dist2+np.linalg.norm(self.velocity)*self.dss.dt*1.)
                #Angle vel to target
                theta=np.abs(np.arctan2(self.velocity[1],self.velocity[0])-np.arctan2(u_vec[1],u_vec[0]))
                if theta>ANG_THRESH: 
                    a=accel_vec+decel_vec
                else:
                    a=accel_vec
                if i==len(waypoints) and brake_cond and stop_flag==True:
                    a=a-normalize(self.velocity)*self.max_accel*2.
                elif i!=len(waypoints) and brake_cond2:
                    a=a-normalize(self.velocity)*self.max_accel*2
                a=normalize(a)*self.max_accel
                #add new velocity vector
                self.velocity=self.velocity+a*self.dss.dt
                #Max velocity condition
                if np.linalg.norm(self.velocity)>self.max_velocity:
                    self.velocity=normalize(self.velocity)*self.max_velocity
                
                #Update position
                self.dss.updatePosition(np.hstack([self.dss.dt*self.velocity,0]))
                self.view.setPosition(self.dss.position)
                #Recompute vectors to points
                u_vec=np.array(p)-self.dss.position[0:2]
                if i>1:
                    u_prev=np.array(waypoints[i-2])-self.dss.position[0:2]
            print np.linalg.norm(self.velocity)
            i+=1
        return self.dss.timer


class diff_drive:
    #TODO Both directions, can move backwards
    view = -1
    dss = -1
    max_velocity = -1
    max_ang_vel = -1
    reset_velocity = -1

    def __init__(self,vrep_view,max_lin_vel,max_ang_vel):
        self.view=vrep_view
        self.dss=model_base(self.view.getPosition(),self.view.getOrientation(),self.view.getTimeStep())
        self.max_velocity = max_lin_vel
        self.max_ang_vel = max_ang_vel
        self.reset_velocity = self.max_velocity

    def followWaypoints(self,waypoints):
        #Maximum step sizes for lin and ang movements
        step_size=self.max_velocity*self.dss.dt
        step_size_ang=self.max_ang_vel*self.dss.dt

        for p in waypoints:

            #Compute vector from position to next waypoint
            u_vec=np.array(p)-self.dss.position[0:2]
            theta=1
            #compute target angle of destination respect position / arctan2?
            target_angle=np.arctan(u_vec[1]/u_vec[0])
            if u_vec[0] < 0 and u_vec[1] >= 0:
                target_angle=target_angle+np.pi
            elif u_vec[0] <0 and u_vec[1] <0:
                target_angle=target_angle-np.pi
            while np.abs(theta)>0:
                #Required rotation
                theta=target_angle-self.dss.orientation[2]
                #Account for both directions of rotation,shortest
                if abs(theta)>np.pi:
                    theta=theta-np.sign(theta)*2*np.pi
                #Limit rotation according to max ang step size
                if abs(theta)>step_size_ang:
                    theta=np.sign(theta)*step_size_ang
                #Update orientation
                self.dss.updateOrientation([0,0,theta])
                #Update view
                self.view.setOrientation(self.dss.orientation)
                self.view.syncSignal()
                self.dss.timer+=self.dss.dt

            while max(abs(u_vec))>0:
                if np.linalg.norm(u_vec)>step_size:
                    #Update position
                    self.dss.updatePosition([step_size*np.cos(self.dss.orientation[2]),step_size*np.sin(self.dss.orientation[2]),0])
                else:
                    self.dss.updatePosition(np.hstack([u_vec,0]))
                #Update view
                self.view.setPosition(self.dss.position)
                #Recompute target dir vec
                u_vec=np.array(p)-self.dss.position[0:2]
        #print(self.dss.timer)
        return self.dss.timer

class kin_car:
    #TODO Both directions, can move backwards
    view = -1
    dss = -1
    L = -1
    max_velocity = -1
    max_ang = -1
    reset_velocity = -1
    step_size = -1
    step_size_ang = -1

    def __init__(self,vrep_view,max_lin_vel,max_ang, length):
        self.view=vrep_view
        self.dss=model_base(self.view.getPosition(),self.view.getOrientation(),self.view.getTimeStep())
        self.L = length
        self.max_ang = max_ang
        self.max_velocity = max_lin_vel
        self.reset_velocity = self.max_velocity
        self.step_size=self.max_velocity*self.dss.dt
        min_rad=self.L/np.tan(self.max_ang)
        self.step_size_ang=self.max_velocity/min_rad*self.dss.dt

    def headingNext(self,p_next):
        u_vec=p_next-self.dss.position[0:2]
        theta=np.arctan2(u_vec[1],u_vec[0])
        if np.abs(theta)<self.step_size_ang:
            return False
        else:
            return True

    def followWaypoints(self,waypoints):
        #Maximum step sizes for lin and ang movements



        i=1
        for p in waypoints:

            #Compute vector from position to next waypoint
            u_vec=np.array(p)-self.dss.position[0:2]
            next_hidden=True
            while max(abs(u_vec))>self.step_size and next_hidden:
                #compute target angle of destination respect position
                target_angle=np.arctan(u_vec[1]/u_vec[0])
                if u_vec[0] < 0 and u_vec[1] >= 0:
                    target_angle=target_angle+np.pi
                elif u_vec[0] <0 and u_vec[1] <0:
                    target_angle=target_angle-np.pi
                #Required rotation
                theta=target_angle-self.dss.orientation[2]
                #Account for both directions of rotation, shortest
                if abs(theta)>np.pi:
                    theta=theta-np.sign(theta)*2*np.pi
                #Limit rotation according to max ang step size
                if abs(theta)>self.step_size_ang:
                    theta=np.sign(theta)*self.step_size_ang
                #Update orientation
                self.dss.updateOrientation([0,0,theta])
                if np.linalg.norm(u_vec)>self.step_size:
                    u_vec=[self.step_size*np.cos(self.dss.orientation[2]),self.step_size*np.sin(self.dss.orientation[2]),0]
                else:
                    u_vec=[np.linalg.norm(u_vec)*np.cos(self.dss.orientation[2]),np.linalg.norm(u_vec)*np.sin(self.dss.orientation[2]),0]
                #Update position
                self.dss.updatePosition(u_vec)
                #Update view
                self.view.setOrientation(self.dss.orientation)
                self.view.setPosition(self.dss.position)
                #Recompute target dir vec
                u_vec=np.array(p)-self.dss.position[0:2]
                if i!=len(waypoints):
                    next_hidden=self.headingNext(waypoints[i])
            i+=1
        return self.dss.timer



class dyn_car:
    view = -1
    dss = -1
    dt = -1
    velocity = -1
    L=-1
    reset_velocity = -1

    max_velocity = -1
    max_accel = -1
    max_ang = -1
    step_size = -1
    step_size_ang= -1


    #init function, Name's are the names given to objects in v-rep
    def __init__(self,vrep_view,max_velocity,max_accel,max_ang, length):
        self.view=vrep_view
        self.dss=model_base(self.view.getPosition(),self.view.getOrientation(),self.view.getTimeStep())
        self.L = length
        self.velocity=0
        self.max_accel = max_accel
        self.max_ang = max_ang
        self.max_velocity = max_velocity
        self.reset_velocity = self.max_velocity
        self.step_size=self.velocity*self.dss.dt

    def headingNext(self,p_next):
        u_vec=p_next-self.dss.position[0:2]
        theta=np.arctan2(u_vec[1],u_vec[0])
        if np.abs(theta)<self.step_size_ang*1.5:
            return False
        else:
            return True

    def followWaypoints(self,waypoints,path_length,stop_flag):
        i=1
        path=0
        vel_vec=[]
        for p in waypoints:

            #Compute difference to actual waypoint
            u_vec=np.array(p)-self.dss.position[0:2]
            next_hidden=True
            #Max angle change x frame
            self.step_size_ang=self.velocity/self.L*np.tan(self.max_ang)*self.dss.dt
            #Step in direction to new point
            while max(abs(u_vec))>self.step_size and next_hidden:
                #Compute target angle
                target_angle=np.arctan(u_vec[1]/u_vec[0])
                if u_vec[0] < 0 and u_vec[1] >= 0:
                    target_angle=target_angle+np.pi
                elif u_vec[0] <0 and u_vec[1] <0:
                    target_angle=target_angle-np.pi
                #Required rotation
                theta=target_angle-self.dss.orientation[2]
                #Account for both directions of rotation,shortest
                if abs(theta)>np.pi:
                    theta=theta-np.sign(theta)*2*np.pi
                #Limit rotation according to max ang step size
                if abs(theta)>self.step_size_ang:
                    theta=np.sign(theta)*self.step_size_ang
                #Update orientation
                self.dss.updateOrientation([0,0,theta])
                #Add vel step towards dest
                self.velocity=self.velocity+self.max_accel*self.dss.dt
                #Compute break distance
                brake_dist=np.power(self.velocity,2)/2/self.max_accel
                #Brake if last waypoint and under brake dist plus one step size
                if abs(path_length-path)<abs(brake_dist) and stop_flag==True:
                    self.velocity=self.velocity-self.max_accel*2*self.dss.dt
                    if self.velocity<self.max_velocity/20:
                        next_hidden=False
                #Limit max velocity
                if self.velocity>self.max_velocity:
                    self.velocity=self.max_velocity
                self.step_size=self.velocity*self.dss.dt
                vel_vec=vel_vec+[self.velocity]
                #Update position
                if np.linalg.norm(u_vec)>self.step_size or i!=len(waypoints):
                    self.dss.updatePosition([self.velocity*self.dss.dt*np.cos(self.dss.orientation[2]),self.velocity*self.dss.dt*np.sin(self.dss.orientation[2]),0])
                    path+=self.velocity*self.dss.dt
                else:
                    self.dss.updatePosition(np.hstack([u_vec,0]))
                    path+=np.linalg.norm(u_vec)
                #Update views
                self.view.setOrientation(self.dss.orientation)
                self.view.setPosition(self.dss.position)
                #Recompute target dir vec
                u_vec=np.array(p)-self.dss.position[0:2]
                #Max angle change x frame
                self.step_size_ang=self.velocity/self.L*np.tan(self.max_ang)*self.dss.dt
#                if i!=len(waypoints):
#                    next_hidden=self.headingNext(waypoints[i])

            i+=1
        #print(self.dss.timer)
        return self.dss.timer, path, vel_vec
#################################################################
#################################################################
#################################################################



if __name__ == "__main__": #so these dont work when simply importing this file but work when you run this file
#    cid = startVrep()
    view = ps.TracingView([],[],[])
#    view = vrep_view(cid,'Cuboid',1)
#    view = vrep_view(cid,'Sphere',1)
#    dss = discrete_statespace(view)
#    kinp = kin_point(view,1)
    model = dyn_point(view,10,0.1)
#    model = diff_drive(view,1,2)
#    model = kin_car(view,1,np.pi/3,5)
#    model = dyn_car(view,100,1,np.pi/4,50)

    model.dss.setPosition([0,0,0.05])

    time.sleep(2) #switch to vrep

    #Vector of waypoints
    #waypoints=[[0,0],[4,0]]
    waypoints=[[100,30],[100,50],[0,100]]

    #Draw waypoints
#    view.drawPoints(waypoints)

    #Follow waypoints
    #dss.followWaypoints(waypoints)
#    kinp.followWaypoints(waypoints)
#    ps.drawPath(kinp.view.pos_trace)
    model.followWaypoints(waypoints,True)
    ps.drawPath(model.view.pos_trace)
#    model.followWaypoints(waypoints)
#    model.followWaypoints(waypoints)
#    model.followWaypoints(waypoints,10,True)


#    stopVrep(cid)

