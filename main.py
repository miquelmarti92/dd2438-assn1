# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 22:27:10 2016

@author: kkalem
"""

import vrep
import sys
import matlabHandler as mh
import polySearch as ps
import waypoint as wp
import discSearch as ds
import numpy as np
import matplotlib.pyplot as plt
import time

#################################################################
#################################################################
#################################################################

def startVrep():
    vrep.simxFinish(-1) # just in case, close all opened connections
    clientID = vrep.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to V-REP
    if clientID != -1:
        return clientID
    else:
        print 'Can not connect to v-rep'
        sys.exit('Connection failed')

def stopVrep(clientID):
    # Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
    vrep.simxGetPingTime(clientID)
    # Now close the connection to V-REP:
    vrep.simxFinish(clientID)

#initilazies the vrep connection, runs the given mainFunc, terminates the connection
def runInVrep(mainFunc):
    clientID = startVrep()
    mainFunc(clientID)
    stopVrep(clientID)


#################################################################
#################################################################
#################################################################

if __name__ == "__main__":
    doThingsInVrep = False
    discrete = False
    discNay = 16
    model = 0

    if discrete: #do discrete stuff
        if doThingsInVrep: #in vrep / load maze_wall scene first
            cid = startVrep()
            #loads and generates the discrete obstacle map in v-rep
            err, wallHandle = vrep.simxGetObjectHandle(cid, 'maze_wall', vrep.simx_opmode_oneshot_wait)
            s, g, m = mh.generateDiscreteMaze(cid, wallHandle,'discObst.mat')
            res = list(reversed(ds.astar(s,g,m,discNay)))
            #Create vrep view object, second parameter is scaling factor of vrep view
            view = wp.vrep_view(cid,'port_sphere',0.1)
            view.setPosition(np.hstack([s,0.05]))
            view.drawPoints(res)
            dss = wp.discrete_statespace(view)
            dist=dss.followWaypoints(res)
            print dist
            stopVrep(cid)
        else: #in plots
            s, g, m = mh.getDiscreteObstacleMap('discObst.mat')
            res = ds.astar(s,g,m,discNay)
            ds.plotMap(m,s,g,np.array(res))
            le = 0
            for i in range(1,len(res)):
                le += ps.euclidDistance(res[i],res[i-1])

    else:
        s, g, x, y, edges, button = mh.generateOBJstr('polygObstTest.mat')
        visRes = ps.astar(s,g,x,y,edges,button)
        visRes.reverse()
        visRes=visRes[0:-1]
        polygons = ps.polysToVerts(x,y,button)
        subdivcnt = 3.0
        targetcnt = 10
        if model==0:
            # 11 kinematic point
            view = ps.TracingView(x,y,edges)
            model = wp.kin_point(view,ps.__kin_pnt_vmax)
            model.dss.setPosition(ps.in3d(s))
            model.followWaypoints(visRes)
            mh.draw2Dpolygons(x,y,edges,False)
            ps.drawPath(model.view.pos_trace)
            time_steps = len(model.view.pos_trace)
            total_time = time_steps * ps.__plotview_time_step
            print 'total time of kinematic point:' + str(total_time)
        elif model ==1:
            #12 dynamic point fails horribly most of the time
            view = ps.TracingView(x,y,edges)
#            cid = startVrep()
#            view =wp.vrep_view(cid,'Sphere',0.01)
#            model = wp.dyn_point(view,ps.__dyn_pnt_vmax_a,ps.__dyn_pnt_amax_b)
#            model.dss.setPosition(ps.in3d(s))
#            dynRes, wps = ps.correctiveSearch(visRes,model, polygons, s, subdivcnt, targetcnt)

            model = wp.dyn_point(view,ps.__dyn_pnt_vmax_a,ps.__dyn_pnt_amax_a)
            model.dss.setPosition(ps.in3d(s))
            t=model.followWaypoints(visRes,True)
#            mh.draw2Dpolygons(x,y,edges,False)
            ps.drawPath(model.view.pos_trace)
            time_steps = len(model.view.pos_trace)
            total_time = time_steps * ps.__plotview_time_step
            print 'total time of dynamic point:' + str(total_time) #y u not use my lovely built in timers
#            print str(t)
#            stopVrep(cid)
        elif model==2:
            #13 diff drive turns on spot, follow visibility path
            view = ps.TracingView(x,y,edges)
            model = wp.diff_drive(view,ps.__dif_vmax,ps.__dif_wmax)
            model.dss.setPosition(ps.in3d(s))
            t=model.followWaypoints(visRes)
            mh.draw2Dpolygons(x,y,edges,False)
            ps.drawPath(model.view.pos_trace)
#            time_steps = len(model.view.pos_trace)
#            total_time = time_steps * ps.__plotview_time_step
#            print 'total time of differential drive:' + str(total_time) # Differs from my timer, not counting when turns on spot
            print 'total time of differential drive:' + str(t)
        elif model==3:
            #14 kinematic car can't do any curve at all
            view = ps.TracingView(x,y,edges)
#            model = wp.kin_car(view,ps.__kin_car_vmax, ps.__kin_car_phimax,ps.__kin_car_La)
#            model.dss.setPosition(ps.in3d(s))
#            dynRes, wps = ps.correctiveSearch(visRes,model, polygons, s, subdivcnt, targetcnt)

            model = wp.kin_car(view,ps.__kin_car_vmax, ps.__kin_car_phimax,ps.__kin_car_La)
            model.dss.setPosition(ps.in3d(s))
            searchRes = ps.sampleSearch(s,g,visRes,polygons,10,20,model)

            plt.scatter(searchRes[:,0],searchRes[:,1], color = 'r', marker = 'x')

            view = ps.TracingView(x,y,edges)
            timer_model = wp.kin_car(view,ps.__kin_car_vmax, ps.__kin_car_phimax,ps.__kin_car_La)
            timer_model.dss.setPosition(ps.in3d(s))
            t = timer_model.followWaypoints(searchRes.tolist())
            ps.drawPath(timer_model.view.pos_trace)
            print 'total time of kinematic car:' + str(t)
        elif model ==4:
            #15 dynamic car

            view = ps.TracingView(x,y,edges)
            model = wp.dyn_car(view,ps.__dyn_car_vmax, ps.__dyn_car_amax,ps.__dyn_car_phimax,ps.__dyn_car_La)
            model.dss.setPosition(ps.in3d(s))
            searchRes = ps.sampleSearch(s,g,visRes,polygons,10,20,model)
            model.dss.setPosition(ps.in3d(s))
            model.dss.setOrientation([0,0,0])
            a,path_length,vel1=model.followWaypoints(searchRes.tolist(),0,False)
            ps.drawPath(model.view.pos_trace)
            plt.scatter(searchRes[:,0],searchRes[:,1], color = 'r', marker = 'x')
#            view = ps.TracingView(x,y,edges)
            cid = startVrep()
            view =wp.vrep_view(cid,'Cuboid',0.1)
            view.drawPoints(searchRes.tolist())
            timer_model = wp.dyn_car(view,ps.__dyn_car_vmax, ps.__dyn_car_amax,ps.__dyn_car_phimax,ps.__dyn_car_La)
            timer_model.dss.setPosition(ps.in3d(s))
            timer_model.dss.setOrientation([0,0,0])
            t,path,vel2= timer_model.followWaypoints(searchRes.tolist(),path_length,True)
#            ps.drawPath(timer_model.view.pos_trace)
            stopVrep(cid)
            print 'total time of dynamic car:' + str(t)
